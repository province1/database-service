A database service providing queries. This is done for security reasons: 1) Run separately from the backend application allowing for whitelisting a specific IP address, 2) MongoDB Atlas can use whitelisting rather than allowing requests from any origin.

The service is deployed with PM2:

pm2 start ecosystem.config.js --env production

(or use development)

Start using:
npm run dev (development)
npm run prod (production)

Results should be passed to the consuming service in this format:
{error: boolean, ...}

If there is no error, error is set to true, and the result is includeded:
{error: true, result: [result]}

If there is an error, and error_msg should be included along with a code:
{error: true, error_msg: [error message], error_code: [error code number]}

Error Codes:
1: validation error - Something was wrong with the parameters provided in the body. The error message should include details about this.

2: MongoDB error - Mongodb error.

**Important**: all environment variables must be stored in ecosystemc.config.js. This file must never committed to the repository.
